<?php
  error_reporting(E_ALL);
  ini_set('display_errors', '1');
  error_reporting(0);
  include "./app/inc/security.php";
  include "./app/inc/funtions.php";
  require_once "./app/inc/db.php";
  $question=0;
  $db = new ChefDB();
  if (isset($_POST['dni'])  AND isset($_POST['date']) ) {
      $doc = test_input2($_POST['dni']);
      $cod = test_input2($_POST['date']);
      $db->postLogin($doc, $cod);
  }else {
      if ($_SESSION["id"] == "" AND $_SESSION["name"] == "" and $_SESSION["segmento"] == "") {
        header('Location:' . $exit);//si existe la sesión
        exit;
      }
  }

  if ($_SESSION["tipo"]=="2") {
        header('Location:' . $site.'clasificacion-equipo.php');//
  }

  if ($_SESSION["tipo"]=="1" AND $_SESSION["school"]!="0" ) {
      $question=1;
  }
  $school= $db->postSchool();
  if (isset($_GET['school'])) {
    $getschool = test_input2($_GET['school']);
    $db->postRegschool($_SESSION["id"],$getschool);
    $question=1;
  }
  if ($_SESSION["goles"]>=1000){
    $goles=1130;
  } else{
    $goles=$_SESSION["goles"];
  }
  //award_active       //puede redimir
  //item_redeemed     //ya redimio
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clasificación individual</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/responsive.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-NKQGRDJ');</script>
    <!-- End Google Tag Manager -->
  </head>
  <body class="int-page">
    <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKQGRDJ"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
		<header>
			<div class="container">
        <nav class="menu_type-player">
          <ul class="list_inline">
            <li class="item_menu item_active"><a href="#">JUEGO INDIVIDUAL</a></li>
            <?php
                if ($question=="1") {
                  echo '<li class="item_menu"><a href="./clasificacion-equipo.php">JUEGO EN EQUIPO</a></li>';
                }
             ?>

          </ul>
        </nav>
        <div class="logo">
          <a href="#">
            <img class="img-responsive" src="images/logo2.svg" alt="BBVA Continental">
          </a>
        </div>
        <div class="ico-action open-menu visible-xs visible-sm"><img src="images/icons/ico-open.svg" alt="Cerrar menú"></div>
        <nav class="main_menu">
          <ul class="menu_main list_inline">
            <div class="ico-action close-menu visible-xs visible-sm"><img src="images/icons/ico-close.svg" alt="Cerrar menú"></div>
            <li class="item_menu item_active">
              <div class="btn-scroll" data-goto="mis-goles" href="">Mis goles</div>
            </li>
            <li class="item_menu">
              <div class="btn-scroll" data-goto="premios" href="">Premios</div>
            </li>
            <li class="item_menu">
              <div class="btn-scroll" data-goto="como-jugar" href="">¿Cómo jugar?</div>
            </li>
            <li class="item_menu">
              <a  href="<?php echo $site ?>exit.php">Cerrar sesión</a>
            </li>
          </ul>
        </nav>
      </div>
		</header>
    <section id="classification" class="qualification_content bg_cover" style="background: url('images/backgrounds/bg-qualification.png') center no-repeat">
      <div class="container">
        <section class="content_whith-medium">
          <h1 class="title_section-white">Hola, Luis</h1>
          <p class="text_basic-white">Marca la mayor cantidad de goles y viaja a la</p>
          <div class="img_cup"><img class="img-responsive" src="images/icons/ico-copa-graficsvg.svg" alt=""></div>
          <div class="your_goals">
            <h4 class="title_section-white">Estos son los goles que has marcado</h4>
            <ul class="dats_goals">
              <li class="item_goals item_bg"><?php  echo $_SESSION["name"]; ?></li>
              <li class="item_goals"><?php  echo $_SESSION["goles"]; ?></li>
              <li class="item_goals">goles</li>
            </ul>
          </div>
        </section>
        <section class="marker_content">
          <article class="text_basic-white text-ppal">
            <p>*Última actualización de goles <strong>Marzo 25</strong> de 2019</p>
            <p>Suma <strong>goles</strong> comprando con tu <strong>Tarjeta del Hincha Débito y/o Crédito Mastercard</strong> y participa por un viaje a la <strong>CONMEBOL Copa América Brasil 2019.</strong></p>
          </article>
          <h1 class="title_section-white text_center">Este es tu marcador</h1>
          <div class="grafic_marker">
            <ul class="marker_values">
              <li class="item_market value1">
                <p>250</p>
              </li>
              <li class="item_market value2">
                <p>500</p>
              </li>
              <li class="item_market value3">
                <p>750</p>
              </li>
              <li class="item_market value4">
                <p>1000</p>
              </li>
            </ul>
            <div class="ico-ball" style="width:<?php  echo $goles; ?>px">
              <img class="img-reponsive" src="images/icons/ico-balon.svg" alt="Balón" class="img-responsive">
            </div>
            <div class="ico-arc"><img class="img-reponsive" src="images/icons/ico-arco.svg" alt="Portería"></div>
          </div>
          <article class="text_note">*Recuerda que por cada S/10 en compras con tu Tarjeta del Hincha Débito y/o crédito, acumulas un gol.</article>
        </section>
        <section class="awards_content">
          <h1 id="premios" class="title_section-white text_center">Marca goles y desbloquea premios</h1>
          <ul class="list_awards list_inline">
            <li class="item_awards <?php if ($_SESSION["goles"]>=250){echo ".award_active ";}?>">
              <div class="ico_starts"><img src="images/icons/ico-start-disable1.svg" alt="Estrella 2"></div>
              <div class="ico_cup"><img src="images/icons/ico-cup.svg" alt=""></div>
              <h1 class="title_section-white">100<br><small>Goles</small></h1>
              <div data-toggle="modal" data-target="#modalpremios1" class="btn_basic-bluedark">Ver premios</div>
            </li>
            <li class="item_awards <?php if ($_SESSION["goles"]>=500){echo ".award_active ";}?>">
              <div class="ico_starts"><img src="images/icons/ico-start-disable2.svg" alt="Estrella 2"></div>
              <div class="ico_cup"><img src="images/icons/ico-lock.svg" alt=""></div>
              <h1 class="title_section-white">200<br><small>Goles</small></h1>
              <div data-toggle="modal" data-target="#modalpremios2" class="btn_basic-bluedark">Ver premios</div>
            </li>
            <li class="item_awards <?php if ($_SESSION["goles"]>=750){echo ".award_active ";}?>">
              <div class="ico_starts"><img src="images/icons/ico-start-disable3.svg" alt="Estrella 2"></div>
              <div class="ico_cup"><img src="images/icons/ico-lock.svg" alt=""></div>
              <h1 class="title_section-white">350<br><small>Goles</small></h1>
              <div data-toggle="modal" data-target="#modalpremios3" class="btn_basic-bluedark">Ver premios</div>
            </li>
            <li class="item_awards <?php if ($_SESSION["goles"]>=1000){echo ".award_active ";}?>">
              <div class="ico_starts"><img src="images/icons/ico-start-disable4.svg" alt="Estrella 2"></div>
              <div class="ico_cup"><img src="images/icons/ico-lock.svg" alt=""></div>
              <h1 class="title_section-white">500<br><small>Goles</small></h1>
              <div data-toggle="modal" data-target="#modalpremios4" class="btn_basic-bluedark">Ver premios</div>
            </li>
          </ul>
        </section>
      </div>
    </section>
    <section id="como-jugar" class="how-play content_section bg_cover" style="background: url('images/backgrounds/bg-howplay.png') center no-repeat;">
      <div class="container-fluid">
        <h1 class="title_section">¿Cómo jugar?</h1>
        <div id="individual" class="tab-pane fade in active">
            <div class="row">
              <div class="col-lg-6 col-md-6 n_padding-left">
                <div class="img_tabs"><img class="img-responsive" src="images/backgrounds/bg_individualplay.png" alt="Imagen jugador individual"></div>
              </div>
              <div class="col-lg-6 col-md-6">
                <ul class="list_indcations">
                  <li class="item_howplay">
                    <div class="count_list">1</div>
                    <h4 class="title_item">Comienza a jugar</h4>
                    <article class="text_item">¡No necesitas registrarte! Para empezar solo debes ingresar tu DNI <a href="">aquí</a> y así podrás consultar el estado actual de tus goles acumulados por usar tu Tarjeta del Hincha Débito y/o Crédito Mastercard.</article>
                  </li>
                  <li class="item_howplay">
                    <div class="count_list">2</div>
                    <h4 class="title_item">¿Cómo marcar goles?</h4>
                    <article class="text_item">Haz tu jugada individual comprando en diferentes comercios y por cada S/10 en compras marcas un gol que te llevará más cerca de la CONMEBOL Copa América Brasil 2019.</article>
                  </li>
                  <li class="item_howplay">
                    <div class="count_list">3</div>
                    <h4 class="title_item">Metas</h4>
                    <article class="text_item">Podrás redimir tus premios cuando alcances cada meta. Recuerda que aunque redimas todos tus premios, los goles que has acumulado no se descontarán sino que seguirán sumando para tener la posibilidad de acompañar a la Selección en la CONMEBOL Copa América Brasil 2019.</article>
                  </li>
                </ul>
              </div>
            </div>
          </div>
      </div>
    </section>
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <article class="text_basic-white">
              <strong>Mastercard</strong> is the <strong>Official Sponsor of CONMEBOL Copa América Brasil 2019.</strong> Promoción válida del 1ro de enero de 2019 al 5 de mayo de 2019. Para más información acceder a las bases de la promoción publicadas en <a href="www.el11somostodos.pe" target="_blank">www.el11somostodos.pe</a> Aplican condiciones y restricciones
            </article>
            <ul class="list_footer list_inline">
              <li class="item_footer">
                <a href="#">Preguntas frecuentes</a>
              </li>
              <li class="item_footer">
                <a href="#">Términos y condiciones</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="logos">
              <img class="img-responsive content-left" src="images/logos-footer.png" alt="BBVA Continental">
              <img class="img-responsive logo-relative" src="images/logo2.svg" alt="">
            </div>
          </div>
        </div>
      </div>
    </footer>

    <div id="modalpremios1" class="modal fade modal_home modal_qualification" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="header_popup">
            <div class="icons_starts"><img src="images/icons/ico-start-disable1.svg" alt="Estrella 1"></div>
            <h1 class="title_section-white">100 goles</h1>
          </div>
          <ul class="list_bonos list_inline">
            <li class="item_bono">
              <h4 class="text_basic-white">Amazon</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/amazon.jpg" alt="Bono 1"></div>
              <article class="description_bono">S/50,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Repsol</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/repsol.jpg" alt="Bono 2"></div>
              <article class="description_bono">S/50,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Cineplanet</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/cineplanet.png" alt="Bono 3"></div>
              <article class="description_bono">S/50,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Tony Roma´s</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/tony-romas.jpg" alt="Bono 4"></div>
              <article class="description_bono">S/50,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div id="modalpremios2" class="modal fade modal_home modal_qualification" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="header_popup">
            <div class="icons_starts"><img src="images/icons/ico-start-disable1.svg" alt="Estrella 1"></div>
            <h1 class="title_section-white">200 goles</h1>
          </div>
          <ul class="list_bonos list_inline">
            <li class="item_bono">
              <h4 class="text_basic-white">Amazon</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/amazon.jpg" alt="Bono 1"></div>
              <article class="description_bono">S/100,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Repsol</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/repsol.jpg" alt="Bono 2"></div>
              <article class="description_bono">S/100,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Tailoy</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/tai-liy.jpg" alt="Bono 3"></div>
              <article class="description_bono">S/100,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Cineplanet</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/cineplanet.png" alt="Bono 4"></div>
              <article class="description_bono">S/100,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div id="modalpremios3" class="modal fade modal_home modal_qualification" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="header_popup">
            <div class="icons_starts"><img src="images/icons/ico-start-disable1.svg" alt="Estrella 1"></div>
            <h1 class="title_section-white">350 goles</h1>
          </div>
          <ul class="list_bonos list_inline">
            <li class="item_bono">
              <h4 class="text_basic-white">Amazon</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/amazon.jpg" alt="Bono 1"></div>
              <article class="description_bono">S/250,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Repsol</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/repsol.jpg" alt="Bono 2"></div>
              <article class="description_bono">S/250,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">FIFA 2019 EA</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/fifa2019.jpg" alt="Bono 3"></div>
              <article class="description_bono">S/250,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Camiseta Selección</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/bono4.png" alt="Bono 4"></div>
              <article class="description_bono">S/250,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div id="modalpremios4" class="modal fade modal_home modal_qualification" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="header_popup">
            <div class="icons_starts"><img src="images/icons/ico-start-disable1.svg" alt="Estrella 1"></div>
            <h1 class="title_section-white">500 goles</h1>
          </div>
          <ul class="list_bonos list_inline">
            <li class="item_bono">
              <h4 class="text_basic-white">Amazon</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/amazon.jpg" alt="Bono 1"></div>
              <article class="description_bono">S/350,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Repsol</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/repsol.jpg" alt="Bono 2"></div>
              <article class="description_bono">S/350,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Tailoy</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/tai-liy.jpg" alt="Bono 3"></div>
              <article class="description_bono">S/350,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
            <li class="item_bono">
              <h4 class="text_basic-white">Camiseta Selección  Firmada</h4>
              <div class="img_bono"><img class="img-responsive" src="images/bonos/bono4.png" alt="Bono 4"></div>
              <article class="description_bono">S/350,00</article>
              <div class="btn_basic-bluelight">Redimir</div>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="popupsbtns">
      <div id="modal5" class="modal fade modal_home" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content" data-parent="juegaEnGrupo">
            <h1 class="title_section">¿Quieres jugar también en equipo?</h1>
            <article class="description_basic">La opción que escojas será definitiva. Si escoges no seguir jugando, no podrás cambiar tu decisión después. Si escoges jugar en equipo, empezarás a marcar goles con las compras que hagas con tu <strong>Tarjeta del Hincha Débito o Crédito Mastercard.</strong></article>
            <div class="btns_options">
              <div class="btn_basic stop_steps btn_basic-gray fclick" data-option="confinmarcionSolo">NO, PREFIERO SOLO</div>
              <div class="btn_basic next-step btn_basic-light fclick" data-option="seleccionEquipo">SI, JUGAR AHORA</div>
            </div>
          </div>
          <div class="modal-content hidden" data-parent="confinmarcionSolo">
            <h1 class="title_section">¿Estás seguro de que NO quieres jugar en equipo?</h1>
            <div class="btns_options">
              <div class="btn_basic stop_steps btn_basic-gray fclick" data-option="juegaEnGrupo">MEJOR EN EQUIPO</div>
              <div class="btn_basic next-step btn_basic-light fclick" data-option="exito">
                      <a href="" class="linkschool">
                            ESTOY SEGURO
                    </a>
                </div>
            </div>
          </div>
          <div class="modal-content hidden" data-parent="seleccionEquipo">
            <h1 class="title_section">Escoge el colegio por el cual vas a jugar</h1>
            <select name="school" id="school">
                <option>Seleccionar</option>
                <?php
                    while($schoolitem= $school->fetch_array()) {
                        echo '<option value='.$schoolitem[0].'>'.$schoolitem[3].'</option>';
                    }
                ?>
            </select>
            <div class="btns_options">
              <div class="btn_basic stop_steps btn_basic-gray fclick" data-option="juegaEnGrupo">VOLVER</div>
              <div class="btn_basic next-step btn_basic-light fclick" data-option="confinmarcionEquipo">EMPEZAR A JUGAR</div>
            </div>
          </div>
          <div class="modal-content hidden" data-parent="confinmarcionEquipo">
            <h1 class="title_section">Has escogido jugar con</h1>
            <h3 class="title_light nameschool">Nombre colegio</h3>
            <article class="text_basic-dark">¿Estás seguro?</article>
            <div class="btns_options">
              <div class="btn_basic stop_steps btn_basic-gray fclick" data-option="seleccionEquipo">ESCOGER OTRO</div>
              <div class="btn_basic next-step btn_basic-light fclick" data-option="exito">
                <a href="" class="linkschool">
                      ESTOY SEGURO
                </a>
              </div>
            </div>
          </div>
          <div class="modal-content hidden" data-parent="exito">
            <h1 class="title_section">¡Tu elección ha sido registrada con éxito!</h1>
            <article class="text_basic-dark">Podrás ver los goles y la información de tu equipo en la próxima actualización.</article>
            <div class="btns_options">
              <div class="btn_basic next-step btn_basic-light fclick" data-option="home">continuar</div>
            </div>
          </div>
        </div>
      </div>
      <div id="modal4" class="modal fade modal_home" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content" >
            <div class="row">
            <h1 class="title_section">¿Estás seguro que quieres redimir este premio?</h1>
            <div class="col-xs-12 col-sm-6">
              <img class="img-responsive" src="images/icons/logo-mastercard.svg" alt="Master Card">
            </div>
            <div class="col-xs-12 col-sm-6">
              <article class="text_basic-dark color-gray text-left">La opción que escojas será definitiva. Si escoges no seguir jugando, no podrás cambiar tu decisión después. Si escoges jugar en equipo, empezarás a marcar goles con las compras que hagas con tu <strong>Tarjeta del Hincha Débito o Crédito Mastercard.</strong></article>
            <div class="btns_options tex-center">
              <div class="btn_basic next-step btn_basic-light btn_basic-100">REDIMIR AHORA</div>
              <a class="link-modal">Elegir otro</a>
            </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="js/script.min.js"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          var question= <?php  echo $question; ?>;
          if (question=="0") {
            $("#modal5").modal("show");
          }
          $( "#school" ).change(function() {
            $(".linkschool").attr("href","./clasificacion.php?school="+$(this).val());
            $(".nameschool").text($(this).find(":selected").text());
          });

      });
    </script>
  </body>
</html>

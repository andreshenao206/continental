<?php
  session_start();
  $_SESSION['test1']=md5($_SERVER['HTTP_USER_AGENT']);
  $_SESSION['test2']=md5($_SERVER['REMOTE_ADDR']);
  $use_sts = true;

  // iis sets HTTPS to 'off' for non-SSL requests
  if ($use_sts && isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
      header('X-Frame-Options: DENY');
  } elseif ($use_sts) {
      header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], true, 301);
      // we are in cleartext at the moment, prevent further execution and output
      die();
  }
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home continental</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/responsive.css">
    <style>
        .lastform{
             display:none;
        }
    </style>
    <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-NKQGRDJ');</script>
    <!-- End Google Tag Manager -->

  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKQGRDJ"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
		<header>
			<div class="container">
				<div class="logo">
          <a href="#">
            <img class="img-responsive" src="images/logo2.svg" alt="BBVA Continental">
          </a>
        </div>
        <div class="ico-action open-menu visible-xs visible-sm"><img src="images/icons/ico-open.svg" alt="Cerrar menú"></div>
        <nav class="main_menu">
          <div class="ico-action close-menu visible-xs visible-sm"><img src="images/icons/ico-close.svg" alt="Cerrar menú"></div>
          <ul class="menu_main list_inline">
            <li class="item_menu item_active">
              <a href="#">Ingresar</a>
            </li>
            <li class="item_menu">
              <div class="btn-scroll" data-goto="como-jugar" href="">¿Cómo jugar?</div>
            </li>
          </ul>
        </nav>
			</div>
    </header>
    <section id="home">
      <section class="banner_ppl bg_cover">
        <div class="container">
          <div class="content_banner">
            <h2 class="title_banner">Vamos juntos a la</h2>
            <div class="ico_text">
              <img class="img-responsive" src="images/icons/text-copa.svg" alt="">
              <img class="img-responsive ico-card" src="images/icons/ico-card1.png" alt="">
            </div>
            <form action="./clasificacion.php" class="form_home" method="POST">
              <div class="firstform">
                <label class="arrow-left" for=""></label>
                <input class="show" type="text" placeholder="Ingresa tu DNI" autocomplete="off" name="dni">
                <div class="btn_basic-light btn_form firstbtn">Siguiente</div>
              </div>
                <div class="lastform">
                  <input type="text" id="datepicker" placeholder="Fecha de nacimiento" name="date">
                  <button type="submit" class="btn_basic-light btn_form lastbtn enviar">Ingresar </button>
                </div>
                <article class="text_basic-white">*Participa hasta el 30 de abril de 2019.</article>
            </form>
          </div>
          <div class="logo_text hidden-xs hidden-sm">
            <img src="images/icons/somos-todos.png" alt="11 Somos todos">
          </div>
          <div class="ico-scrool">
            <img src="images/icons/ico-scroll.svg" alt="" class="img-responsive">
          </div>
        </div>

      </section>
      <section id="como-jugar" class="how-play content_section bg_cover" style="background: url('images/backgrounds/bg-howplay.png') center no-repeat;">
        <div class="container-fluid">
          <h1 class="title_section">¿Cómo quieres jugar?</h1>
            <article class="text_basic-blue">Conoce la mecánica del juego individual y equipo y vive toda la emoción de la <strong>CONMEBOL Copa América Brasil 2019.</strong></article>
            <ul class="nav nav-tabs tabs_play">
              <li class="active item_tab"><a data-toggle="tab" href="#individual">JUGAR INDIVIDUAL</a></li>
              <li class="item_tab"><a data-toggle="tab" href="#team">JUGAR EN EQUIPO</a></li>
            </ul>
            <div class="tab-content">
              <div id="individual" class="tab-pane fade in active">
                <div class="row">
                  <div class="col-lg-6 col-md-6 n_padding-left">
                    <div class="img_tabs"><img class="img-responsive" src="images/backgrounds/bg_individualplay.png" alt="Imagen jugador individual"></div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <ul class="list_indcations">
                      <li class="item_howplay">
                        <div class="count_list">1</div>
                        <h4 class="title_item">Comienza a jugar</h4>
                        <article class="text_item">¡No necesitas registrarte! Para empezar solo debes ingresar tu DNI <a href="">aquí</a> y así podrás consultar el estado actual de tus goles acumulados por usar tu Tarjeta del Hincha Débito y/o Crédito Mastercard.</article>
                      </li>
                      <li class="item_howplay">
                        <div class="count_list">2</div>
                        <h4 class="title_item">¿Cómo marcar goles?</h4>
                        <article class="text_item">Haz tu jugada individual comprando en diferentes comercios y por cada S/10 en compras marcas un gol que te llevará más cerca de la CONMEBOL Copa América Brasil 2019.</article>
                      </li>
                      <li class="item_howplay">
                        <div class="count_list">3</div>
                        <h4 class="title_item">Metas</h4>
                        <article class="text_item">Podrás redimir tus premios cuando alcances cada meta. Recuerda que aunque redimas todos tus premios, los goles que has acumulado no se descontarán sino que seguirán sumando para tener la posibilidad de acompañar a la Selección en la CONMEBOL Copa América Brasil 2019.</article>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="team" class="tab-pane fade">
                <div class="row">
                  <div class="col-lg-6 col-md-6 n_padding-left">
                    <div class="img_tabs"><img class="img-responsive" src="images/backgrounds/bg_teamplay.png" alt="Imagen jugador individual"></div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <ul class="list_indcations">
                      <li class="item_howplay">
                        <div class="count_list">1</div>
                        <h4 class="title_item">Comienza a jugar</h4>
                        <article class="text_item">Ingresa con tu DNI <a href="#">aquí</a> y podrás ver los goles marcados de forma grupal e individual. Sólo en esta opción podrás jugar de ambas maneras y sumar goles para el colegio que elegiste y para ti.</article>
                      </li>
                      <li class="item_howplay">
                        <div class="count_list">2</div>
                        <h4 class="title_item">¿Cómo marcar goles?</h4>
                        <article class="text_item">Puedes marcar muchos goles pagando la mensualidad del colegio de tu hijo con la Tarjeta del Hincha Débito y/o Crédito Mastercard y también comprando en diferentes comercios. Por cada S/10 en compras marcas un gol que te permitirá tener el trofeo de la CONMEBOL Copa América en tu colegio.</article>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </section>
    </section>


    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <article class="text_basic-white">
              <strong>Mastercard</strong> is the <strong>Official Sponsor of CONMEBOL Copa América Brasil 2019.</strong> Promoción válida del 1ro de enero de 2019 al 5 de mayo de 2019. Para más información acceder a las bases de la promoción publicadas en <a href="www.el11somostodos.pe" target="_blank">www.el11somostodos.pe</a> Aplican condiciones y restricciones
            </article>
            <ul class="list_footer list_inline">
              <li class="item_footer">
                <a href="#">Preguntas frecuentes</a>
              </li>
              <li class="item_footer">
                <a href="#">Términos y condiciones</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="logos">
              <img class="img-responsive content-left" src="images/logos-footer.png" alt="BBVA Continental">
              <img class="img-responsive logo-relative" src="images/logo2.svg" alt="">
            </div>
          </div>
        </div>
      </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/sha1.js"></script>
<script type="text/javascript">
      $('input[name="dni"]').on('input', function () {//numerio campo
          this.value = this.value.replace(/[^0-9]/g,'');
          this.value =this.value.substr(0,16);
      });
      $(".firstbtn").click(function(){//numerio campo
          //var response = grecaptcha.getResponse();

          var doc= $('input[name="dni"]').val();
        //  if(response.length != 0){
                 //$('input[name="recaptcha"]').val(response);

                 if ($.isNumeric(doc)) {//validamos que sea numerico
                        if (doc.length<=16) {
                          var hash = CryptoJS.SHA1(doc);
                          var result = CryptoJS.enc.Hex.stringify(hash);                          //var encodedString = btoa(String(doc));
                          $('input[name="dni"]').attr('type', 'password');
                          $('input[name="dni"]').val(result);
                          $(".firstform").hide();
                          $(".lastform").show();
                          //document.getElementById("form_login").submit();
                    }else{
                        $("#msgerror").show();
                      //  grecaptcha.reset();
                    }
                 }
          //  }
           return false;
     });
     $(".lastbtn").click(function(){//numerio campo
         //var response = grecaptcha.getResponse();

         var doc= $('input[name="date"]').val();
       //  if(response.length != 0){
                //$('input[name="recaptcha"]').val(response);

                if ($.isNumeric(doc)) {//validamos que sea numerico
                       if (doc.length<=16) {
                         var hash = CryptoJS.SHA1(doc);
                         var result = CryptoJS.enc.Hex.stringify(hash);                          //var encodedString = btoa(String(doc));
                         $('input[name="date"]').attr('type', 'password');
                         $('input[name="date"]').val(result);
                         document.getElementById("form_login").submit();
                   }else{
                       $("#msgerror").show();
                     //  grecaptcha.reset();
                   }
                }
         //  }
          return false;
    });

    $(function() {
      $('#datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
      });
    });
</script>
  </body>
</html>

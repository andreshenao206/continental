<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
//  error_reporting(0);
include "./app/inc/security.php";
include "./app/inc/funtions.php";
require_once "./app/inc/db.php";

$db = new ChefDB();
if ($_SESSION["tipo"]=="1") {
  $results= $db->postSchoolgoals($_SESSION["school"]);
  $results= $results->fetch_row();
  $name=$results[3];
  $goles=$results[7];
}else{
  $name=$_SESSION["name"];
  $goles=$_SESSION["goles"];
}

 ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clasificación en equipo</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-NKQGRDJ');</script>
    <!-- End Google Tag Manager -->
  </head>
  <body class="int-page">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKQGRDJ"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header>
      <div class="container">
        <nav class="menu_type-player">
          <ul class="list_inline">
            <?php
                if ($_SESSION["tipo"]=="1") {
                 echo '<li class="item_menu"><a href="./clasificacion.php">JUEGO INDIVIDUAL</a></li>';
                }
             ?>

            <li class="item_menu item_active"><a href="clasificacion-equipo.html">JUEGO EN EQUIPO</a></li>
          </ul>
        </nav>
        <div class="logo">
          <a href="#">
            <img class="img-responsive" src="images/logo2.svg" alt="BBVA Continental">
          </a>
        </div>
        <div class="ico-action open-menu visible-xs visible-sm"><img src="images/icons/ico-open.svg" alt="Cerrar menú"></div>
        <nav class="main_menu">
          <ul class="menu_main list_inline">
            <div class="ico-action close-menu visible-xs visible-sm"><img src="images/icons/ico-close.svg" alt="Cerrar menú"></div>
            <li class="item_menu item_active">
              <div class="btn-scroll" data-goto="mis-goles" href="">Mis goles</div>
            </li>
            <li class="item_menu">
              <div class="btn-scroll" data-goto="premios" href="">Premios</div>
            </li>
            <li class="item_menu">
              <div class="btn-scroll" data-goto="como-jugar" href="">¿Cómo jugar?</div>
            </li>
            <li class="item_menu">
              <a  href="<?php echo $site ?>exit.php">Cerrar sesión</a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
    <section id="classification" class="qualification_content bg_cover qualification-equipo">
      <div class="container">
        <section class="content_whith-medium">
          <h1 class="title_section-white">San Silvestre School</h1>
            <table class="table_dats table_qualification">
              <tr>
                <th class="description_table" colspan="4">Aquí puedes ver la clasificación general general y cuál es la posición de tu colegio en la tabla</th>
              </tr>
              <tr>
                <td class="text_table text_center">1</td>
                <td class="text_table">Franklin Delano R</td>
                <td class="text_table text_center">3000</td>
                <td class="text_table text_center">Goles</td>
              </tr>
              <tr>
                <td class="text_table text_center">2</td>
                <td class="text_table">Markham College</td>
                <td class="text_table text_center">2500</td>
                <td class="text_table text_center">Goles</td>
              </tr>
              <tr>
                <td class="text_table text_center">3</td>
                <td class="text_table">Newton College</td>
                <td class="text_table text_center">2100</td>
                <td class="text_table text_center">Goles</td>
              </tr>
              <tr>
                <td class="text_table text_center">4</td>
                <td class="text_table">Peruano británico</td>
                <td class="text_table text_center">2100</td>
                <td class="text_table text_center">Goles</td>
              </tr>
            </table>
            <div class="your_goals">
              <h4 class="title_section-white">Esta es la suma de los goles marcados</h4>
              <ul class="dats_goals">
                <li class="item_goals item_bg">San Silvestre School</li>
                <li class="item_goals">2050</li>
                <li class="item_goals">goles</li>
              </ul>
            </div>
        </section>
      </div>
    </section>
    <section class="awards_team bg_cover" style="background: url('images/backgrounds/bg_team_second.png') center no-repeat">
      <div class="container">
      <p class="text_note text-center">*Última actualización de goles <strong>Marzo 25</strong> de 2019</p>
        <h1 id="premios" class="title_section-white">Premios</h1>
        <article class="text_basic-white text_center">Usa tu <strong>Tarjeta del Hincha Débito y/o Crédito Mastercard</strong> y participa por estos increíbles premios para tu colegio</article>
        <h1 id="premios" class="title_section-white ico-cup cup-yellow">Para el primer puesto</h1>
        <ul class="awards_team-list list_inline">
          <li class="item_team">
            <article class="text_basic-white text_ppal">Meet & Greet</article>
            <div class="img_award">
              <img class="img-responsive" src="images/premios/premio1.png" alt="Premio 1">
            </div>
            <article class="text_basic-white text_footer">Podrás conocer a una leyenda del fútbol peruano</article>
          </li>
          <li class="item_team">
            <article class="text_basic-white text_ppal">Partido Padres vs. Viejas glorias</article>
            <div class="img_award">
              <img class="img-responsive" src="images/premios/premio2.png" alt="Premio 1">
            </div>
            <article class="text_basic-white text_footer">Un partido entre Padres vs las Viejas Glorias de la selección Perú en La Videna.</article>
          </li>
          <li class="item_team">
            <article class="text_basic-white text_ppal">Boletas para Despedida de la Selección para la CONMEBOL Copa América Brasil 2019.</article>
            <div class="img_award">
              <img class="img-responsive" src="images/premios/premio3.png" alt="Premio 1">
            </div>
            <article class="text_basic-white text_footer">Un palco para 8 personas en el partido de despedida de la selección Perú hacia la Conmebol Copa América.</article>
          </li>
        </ul>
      </div>
    </section>
    <section class="types_awards">
      <div class="container">
        <h1 class="title_section ico-cup cup-blue cup-blue-second">Para los cuatro primeros puestos</h1>
        <div class="bg_quadrangular" style="background: url(images/backgrounds/bg-quadrangular.png) center no-repeat">
          <h1 class="title_section-white text-center">Cuadrangular por la Copa del Hincha</h1>
          <article class="text_note text-center">Los cuatro primeros colegios que marquen más goles participarán en un torneo de fútbol por la Copa del Hincha.</article>
        </div>
        <h1 class="title_section ico-cup cup-blue">Para todos los participantes</h1>
        <div class="img_types">
          <img src="images/premios/all_participants.png" alt="Todos los participantes" class="img-responsive">
        </div>
        <article class="description_basic-blue">Son 50 entradas en total que serán sorteadas entre los participantes que acumulen 100 goles o más en la tabla de posiciones con su colegio.</article>
      </div>
    </section>
    <section id="como-jugar" class="how-play content_section bg_cover" style="background: url('images/backgrounds/bg-howplay.png') center no-repeat;">
      <div class="container-fluid">
        <h1 class="title_section">¿Cómo jugar?</h1>
        <div id="team">
            <div class="row">
              <div class="col-lg-6 col-md-6 n_padding-left">
                <div class="img_tabs"><img class="img-responsive" src="images/backgrounds/bg_teamplay.png" alt="Imagen jugador individual"></div>
              </div>
              <div class="col-lg-6 col-md-6">
                <ul class="list_indcations">
                  <li class="item_howplay">
                    <div class="count_list">1</div>
                    <h4 class="title_item">Comienza a jugar</h4>
                    <article class="text_item">Ingresa con tu DNI <a href="#">aquí</a> y podrás ver los goles marcados de forma grupal e individual. Sólo en esta opción podrás jugar de ambas maneras y sumar goles para el colegio que elegiste y para ti.</article>
                  </li>
                  <li class="item_howplay">
                    <div class="count_list">2</div>
                    <h4 class="title_item">¿Cómo marcar goles?</h4>
                    <article class="text_item">Puedes marcar muchos goles pagando la mensualidad del colegio de tu hijo con la Tarjeta del Hincha Débito y/o Crédito Mastercard y también comprando en diferentes comercios. Por cada S/10 en compras marcas un gol que te permitirá tener el trofeo de la CONMEBOL Copa América en tu colegio.</article>
                  </li>
                </ul>
              </div>
            </div>
          </div>
      </div>
    </section>
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <article class="text_basic-white">
              <strong>Mastercard</strong> is the <strong>Official Sponsor of CONMEBOL Copa América Brasil 2019.</strong> Promoción válida del 1ro de enero de 2019 al 5 de mayo de 2019. Para más información acceder a las bases de la promoción publicadas en <a href="www.el11somostodos.pe" target="_blank">www.el11somostodos.pe</a> Aplican condiciones y restricciones
            </article>
            <ul class="list_footer list_inline">
              <li class="item_footer">
                <a href="#">Preguntas frecuentes</a>
              </li>
              <li class="item_footer">
                <a href="#">Términos y condiciones</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="logos">
              <img class="img-responsive content-left" src="images/logos-footer.png" alt="BBVA Continental">
              <img class="img-responsive logo-relative" src="images/logo2.svg" alt="">
            </div>
          </div>
        </div>
      </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.min.js"></script>
  </body>
</html>

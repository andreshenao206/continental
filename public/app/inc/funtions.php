<?php
    function logOut() {
        session_unset();
        session_destroy();
        session_start();
        session_regenerate_id(true);
        header('Location: ./');
        exit;
    }
    function test_input($data,$cant) {
        $tama = strlen(stripslashes(htmlspecialchars($data)));
        $data = trim($data);
    		if (preg_match("/(>|=|<+)/", $data)  OR $tama!=$cant  OR is_int($data) ) {
            header('Location: ./');
            exit;
    		}else{
            return $data;
        }
  	}
    function test_input2($data) {
        $tama = strlen(stripslashes(htmlspecialchars($data)));
        $data = trim($data);
    		if (preg_match("/(>|=|<+)/", $data)  ) {
            header('Location: ./');
            exit;
    		}else{
            return $data;
        }
  	}
 ?>

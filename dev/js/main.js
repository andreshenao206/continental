(function ($, window, document, undefined) {
    'use strict';

    function initReady() {
    	
    }

    function initLoad() {
    	$('.item_tab').on('click',function() {
            $('.tabs_play').toggleClass('tabs_action');
        });

        $('.open-menu').on('click',function() {
            $('.main_menu').addClass('main_menu-open');
        });
        
        $('.close-menu').on('click',function() {
            $('.main_menu').removeClass('main_menu-open');
        });
    }

    $(document).on('ready', initReady);
    $(window).on('load', initLoad);

    function scrollToAnchor(idscroll){
        var idContent = $("#"+ idscroll);
        console.log(idContent[0]);
        $('html,body').animate({
            scrollTop: idContent.offset().top
        },'slow');
    }
    
    $(".btn-scroll").click(function(){
        var go = $(this).data('goto');
        scrollToAnchor(go);
    })
    

// local storage pop ups
$('#modal2').modal('show');

if(localStorage.getItem("finalizado") == null){
    $('#modal1').modal('show');
    localStorage.setItem("juegaEnGrupo", null); // creamos las variables en local storage
    localStorage.setItem("seleccionEquipo", null);
    localStorage.setItem("confinmarcionSolo", null);
    localStorage.setItem("confinmarcionEquipo", null);


    $('.fclick').on('click',function(){

        let valorData = $(this).attr('data-option');// capturo el valor del data en el boton
        let positionModal = $(this).parent().parent().attr('data-parent'); //tipo el data del padre que se llama igual a la variable en local storage

        if(valorData == "home"){
            localStorage.setItem("finalizado",true);
            $('#modal1').modal('hide'); // en los resultados finales esconde el pop up
        }else{
            localStorage.setItem(positionModal, valorData); //asigno el valor en el local storage

            let stateModal = "[data-parent='" + valorData + "']"; // este es el pop up que se tiene que abrir
            let cerrar = "[data-parent='" + positionModal + "']"; // este es el pop up que se tiene que cerrar

            $(stateModal).addClass('show').removeClass('hidden'); // modifico la clase hidden
            $(cerrar).removeClass('show').addClass('hidden'); // modifico la clase show
        }
    });

}



})(jQuery, window, document);